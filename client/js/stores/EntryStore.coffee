#!/usr/bin/env coffeenpm 

AppDispatcher = require '../dispatcher/AppDispatcher'
EntryActions = require('../constants/EntryConstants').ActionTypes
UserActions = require('../constants/UserConstants').ActionTypes
EntryActionsCreator = require '../actions/EntryActionsCreator'
UserStore = require './UserStore'
Signal = require('signals').Signal

PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-upsert'
pouchdb = null

change_signal = new Signal() 

purchases = {}
sales = {}

cleanStore = () ->
    purchases = {}
    sales = {}

setupDatabase = (user_id) ->
    pouchdb = new PouchDB user_id
    pouchdb.putIfNotExists
            "_id": "_design/sales"
            "views": 
                "byDay": 
                    "map": "function(doc) {if(doc.date){emit(doc.date.concat(doc.type), [doc.total, 1]);}}"
                    "reduce": "_sum"
                "byItem":
                    "map": "function(doc) {if(doc.items){for(var i=0;i<doc.items.length;i++){var item=doc.items[i];emit([doc.date[0], doc.date[1], doc.date[2], item.item, i], [parseInt(item.quantity,10),item.total]);}}}"
                    "reduce": "_sum"
            "language": "javascript"
        .then (result) ->
            now = new Date()
            getEntryForDay now
        .catch (error) ->
            console.log 'EntryStore create view falied', error

getEntryForDay = (day) ->
    pouchdb.query 'sales/byDay',
            startkey: [day.getFullYear(), day.getMonth(), day.getDate()]
            endkey: [day.getFullYear(), day.getMonth(), day.getDate(), {}]
            group: true
        .then (result) ->
            for row in result.rows
                if row.key[3] == 'sale'
                    sales[row.key] = row.value[0]
                else if row.key[3] == 'purchase'
                    purchases[row.key] = row.value[0]
            change_signal.dispatch()
        .catch (error) ->
            console.log 'Error loading entries for: ', day, error

savePurchases = (items, date) ->
    for item in items
        item.quantity = parseFloat item.quantity
        item.total = parseFloat item.total
    saveItems items, 'purchase', purchases, date
    
saveSales = (items, date) ->
    for item in items
        item.price = parseFloat item.price
        item.quantity = parseFloat item.quantity
        item.total = parseFloat item.total
    saveItems items, 'sale', sales, date
    
saveItems = (items, type, store, timestamp) ->
    date = new Date(parseInt(timestamp))
    sale_id = type + '_' + timestamp
    total = 0
    doc =
        _id: sale_id
        type: type
        date: [
            date.getFullYear(), 
            date.getMonth(), 
            date.getDate()
        ]
        items: []
    for item in items when item.item isnt '' and item.quantity isnt 0 and item.total isnt 0
        doc.items.push item
        total += item.total
    return false if doc.items.length == 0
    doc.total = total
        
    pouchdb.put doc
        .then (result) =>
            key = doc.date.concat type
            previousTotal = store[key] or 0
            store[key] = previousTotal + doc.total
            EntryActionsCreator.newEntryAdded date
            change_signal.dispatch()  
        .catch (err) ->
            console.log 'error', err
    
EntryStore = 
    dispatchToken: AppDispatcher.register (action) ->
        switch action.type
            when UserActions.SYNC_COMPLETE
                setupDatabase UserStore.getUsername()
            when UserActions.LOGOUT_SUCCESS
                cleanStore()
            when EntryActions.SAVE_PURCAHSES
                savePurchases action.content.items, action.content.date
            when EntryActions.SAVE_SALES
                saveSales action.content.items, action.content.date
            when EntryActions.LOAD_DATE_ENTRIES
                getEntryForDay action.content
            
    getPurchases: () ->
        purchases
        
    getSales: () ->
        sales
            
    changeSignal: change_signal
        
if UserStore.getIsLoggedIn()        
    setupDatabase UserStore.getUsername()

module.exports = EntryStore