#!/usr/bin/env coffeenpm 

AppDispatcher = require '../dispatcher/AppDispatcher'
ActionTypes = require('../constants/UserConstants').ActionTypes
PouchAuth = require '../utils/PouchAuth'
Signal = require('signals').Signal

change_signal = new Signal() 

register_errors = {}
login_errors = {}
    
onRegisterUser = (form_data) ->
    delete form_data['confirm_password']
    PouchAuth.registerUser form_data
            
onRegisterFailed = (error) ->
    register_errors = error
    change_signal.dispatch()
    
onLoginUser = (form_data) ->
    PouchAuth.loginUser form_data
    
onLogoutUser = () ->
    PouchAuth.logoutUser localStorage.getItem 'username'
    
onUserLoggedOut = () ->
    localStorage.setItem 'logged_in', false
    localStorage.setItem 'username', null
    change_signal.dispatch()
            
onLoginFailed = (error) ->
    login_errors = error
    change_signal.dispatch()
        
onUserLoggedIn = (username) ->
    localStorage.setItem 'logged_in', true
    localStorage.setItem 'username', username
    change_signal.dispatch()
    
checkDbSync = () ->
    if UserStore.getIsLoggedIn() then PouchAuth.checkDbSync UserStore.getUsername()
    
UserStore = 
    dispatchToken: AppDispatcher.register (action) ->
        switch action.type
            when ActionTypes.REGISTER_REQUEST
                onRegisterUser action.content
            when ActionTypes.REGISTER_FAILED
                onRegisterFailed action.content
            when ActionTypes.LOGIN_REQUEST
                onLoginUser action.content
            when ActionTypes.LOGOUT_REQUEST
                onLogoutUser()
            when ActionTypes.LOGIN_FAILED
                onLoginFailed action.content
            when ActionTypes.LOGIN_SUCCESS
                onUserLoggedIn action.content
            when ActionTypes.LOGOUT_SUCCESS
                onUserLoggedOut()
            when ActionTypes.CHECK_SYNC
                checkDbSync()
            
    getIsLoggedIn: () ->
        localStorage.getItem('logged_in') == 'true'
        
    getUsername: () ->
        localStorage.getItem 'username'
        
    getRegisterErrors: () ->
        register_errors
        
    getLoginErrors: () ->
        login_errors
            
    changeSignal: change_signal
        
module.exports = UserStore