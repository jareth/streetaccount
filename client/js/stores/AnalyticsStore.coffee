#!/usr/bin/env coffeenpm 

AppDispatcher = require '../dispatcher/AppDispatcher'
UserActions = require('../constants/UserConstants').ActionTypes
EntryActions = require('../constants/EntryConstants').ActionTypes
UserStore = require './UserStore'
Signal = require('signals').Signal

PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-upsert'
pouchdb = null

change_signal = new Signal() 

totals = 
    week: 0
    month: 0
trends = null
quantities = null

cleanStore = () ->
    totals = 
        week: 0
        month: 0
    trends = null
    quantities = null
    

setupDatabase = (user_id) ->
    pouchdb = new PouchDB user_id
    pouchdb.putIfNotExists
            "_id": "_design/analytics"
            "views": 
                "byDay": 
                    "map": "function(doc) {if(doc.date){emit(doc.date, doc.type==='purchase'?[0,doc.total]:[doc.total,0]);}}"
                    "reduce": "_sum"
                "byType": 
                    "map": "function(doc) {if(doc.type==='sale'){for(var i=0;i<doc.items.length;i++){var output={};output[doc.date]=doc.items[i].quantity;emit(doc.items[i].item,output);}}}"
                    "reduce": "_count"
                "byItem": 
                    "map": "function(doc) {if(doc.type==='sale'){for(var i=0;i<doc.items.length;i++){emit(doc.date.concat(doc.items[i].item), doc.items[i].quantity);}}}"
                    "reduce": "_sum"
            "language": "javascript"
        .then (result) ->
            getTotalsFor new Date()
        .catch (error) ->
            console.log 'EntryStore create view falied', error

getTotalsFor = (day) ->
    pouchdb.query 'analytics/byDay',
            startkey: [day.getFullYear(), day.getMonth(), day.getDate() - day.getDay()]
            endkey: [day.getFullYear(), day.getMonth(), day.getDate(), {}]
        .then (result) ->
            if result.rows.length isnt 0
                totals.week = result.rows[0].value[0] - result.rows[0].value[1]
                    
            pouchdb.query 'analytics/byDay',
                startkey: [day.getFullYear(), day.getMonth()]
                endkey: [day.getFullYear(), day.getMonth(), {}]
        .then (result) ->
            if result.rows.length isnt 0
                totals.month = result.rows[0].value[0] - result.rows[0].value[1]
            
            pouchdb.query 'analytics/byDay',
                group: true
        .then (result) ->
            labels = []
            purchase_series = []
            sale_series = []
            total_series = []
            for row in result.rows
                labels.push row.key.join '-'
                sale_series.push row.value[0]
                purchase_series.push -row.value[1]
                total_series.push row.value[0] - row.value[1]
            trends = 
                labels: labels
                series: [
                    purchase_series
                    sale_series
                    total_series
                ]
                
            pouchdb.query 'analytics/byItem',
                group: true
        .then (result) ->
            dates = []
            items = {}
            for row in result.rows
                item_key = row.key[3]
                if not items[item_key]? 
                    items[item_key] = []
                date_key = row.key.slice(0,3).join '-'
                if dates.length is 0 or dates[dates.length-1] isnt date_key
                    dates.push date_key
                items[item_key][dates.length - 1] = row.value
            
            quantities = 
                labels: dates
                series: []
                items: []
            for item_key of items
                quantities.series.push items[item_key]
                quantities.items.push item_key
            
            change_signal.dispatch()
        .catch (error) ->
            console.log 'Error loading entries for: ', day, error
    
AnalyticsStore = 
    dispatchToken: AppDispatcher.register (action) ->
        switch action.type
            when UserActions.SYNC_COMPLETE
                setupDatabase UserStore.getUsername()
            when UserActions.LOGOUT_SUCCESS
                cleanStore()
            when EntryActions.NEW_ENTRY_ADDED
                getTotalsFor new Date()
            
    getTotals: () ->
        totals
        
    getTrends: () ->
        trends
        
    getQuantities: () ->
        quantities
            
    changeSignal: change_signal
        
if UserStore.getIsLoggedIn()        
    setupDatabase UserStore.getUsername()

module.exports = AnalyticsStore