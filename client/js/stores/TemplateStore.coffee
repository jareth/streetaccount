#!/usr/bin/env coffeenpm 

AppDispatcher = require '../dispatcher/AppDispatcher'
TemplateActions = require('../constants/TemplateConstants').ActionTypes
UserActions = require('../constants/UserConstants').ActionTypes
UserStore = require './UserStore'
Signal = require('signals').Signal

PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-upsert'
pouchdb = null

change_signal = new Signal() 

purchases_template = []
sales_template = []

cleanStore = () ->
    purchases_template = []
    sales_template = []

setupDatabase = (user_id) ->
    pouchdb = new PouchDB user_id
    getStoredTemplates()

getStoredTemplates = () ->
    pouchdb.get 'purchase_template'
        .then (result) ->
            purchases_template = result.items
            change_signal.dispatch()  
        .catch (error) ->
            console.log 'failed to get template', error
    pouchdb.get 'sale_template'
        .then (result) ->
            sales_template = result.items
            change_signal.dispatch()  
        .catch (error) ->
            console.log 'failed to get template', error

savePurchasesTemplate = (items) ->
    new_items = []
    for item in items when item.item? and item.item isnt ''
        new_items.push 
            item: item.item
            unit: item.unit
    saveTemplate new_items, 'purchase', (items) -> purchases_template = items
    
saveSalesTemplate = (items) ->
    new_items = []
    for item in items when item.item? and item.item isnt ''
        new_items.push 
            item: item.item
            price: item.price
    saveTemplate new_items, 'sale', (items) -> sales_template = items
    
saveTemplate = (items, type, storeFunction) ->
    timestamp = new Date()
    sale_id = type + '_template'
        
    pouchdb.upsert sale_id, (doc) ->
            doc.items = items
            return doc
        .then (result) =>
            storeFunction items
            change_signal.dispatch()  
        .catch (err) ->
            console.log 'error', err
    
TemplateStore = 
    dispatchToken: AppDispatcher.register (action) ->
        switch action.type
            when UserActions.SYNC_COMPLETE
                setupDatabase UserStore.getUsername()
            when UserActions.LOGOUT_SUCCESS
                cleanStore()
            when TemplateActions.SAVE_PURCAHSES_TEMPLATE
                savePurchasesTemplate action.content
            when TemplateActions.SAVE_SALES_TEMPLATE
                saveSalesTemplate action.content
            
    getPurchasesTemplate: () ->
        purchases_template
        
    getSalesTemplate: () ->
        sales_template
            
    changeSignal: change_signal
        
if UserStore.getIsLoggedIn()        
    setupDatabase UserStore.getUsername()

module.exports = TemplateStore