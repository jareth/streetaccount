#!/usr/bin/env coffeenpm 

LoginValidations =
    email: 
        email: true
        presence: true
    password:
        length:
            minimum: 8
        presence: true

RegisterValidations = 
    name:
        presence: true
    country:
        presence: true
    city:
        presence: true
    email: 
        email: true
        presence: true
    password:
        length:
            minimum: 8
        presence: true
    confirm_password:
        presence: true
        equality: 'password'
    business:
        presence: true
    agree:
        presence: true
        inclusion: 
            within: [true]
            message: '^Please agree to the terms and conditions'
            
module.exports = 
    LoginValidations: LoginValidations
    RegisterValidations: RegisterValidations