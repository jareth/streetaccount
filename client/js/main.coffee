#!/usr/bin/env coffeenpm 
React = require 'react'
ReactDOM = require 'react-dom'

AppRouter = require './AppRouter'

injectTapEventPlugin = require 'react-tap-event-plugin'

# Needed for React Developer Tools
window.React = React

# Needed for onTouchTap
# Can go away when react 1.0 release
# Check this repo:
# https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin()

ReactDOM.render(
    React.createElement(AppRouter),
    document.getElementById 'app'
)