#!/usr/bin/env coffeenpm 

AppDispatcher = require '../dispatcher/AppDispatcher'
ActionTypes = require('../constants/TemplateConstants').ActionTypes

module.exports = 
    savePurchasesTemplate: (data) ->
        AppDispatcher.dispatch
            type: ActionTypes.SAVE_PURCAHSES_TEMPLATE
            content: data
        
    saveSalesTemplate: (data) ->
        AppDispatcher.dispatch
            type: ActionTypes.SAVE_SALES_TEMPLATE
            content: data
        
