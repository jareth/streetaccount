#!/usr/bin/env coffeenpm 

AppDispatcher = require '../dispatcher/AppDispatcher'
ActionTypes = require('../constants/UserConstants').ActionTypes

module.exports = 
    registerUser: (data) ->
        AppDispatcher.dispatch
            type: ActionTypes.REGISTER_REQUEST
            content: data
            
    registerFailed: (data) ->
        AppDispatcher.dispatch
            type: ActionTypes.REGISTER_FAILED
            content: data
        
    loginUser: (data) ->
        AppDispatcher.dispatch
            type: ActionTypes.LOGIN_REQUEST
            content: data
        
    logoutUser: () ->
        AppDispatcher.dispatch
            type: ActionTypes.LOGOUT_REQUEST
        
    loginFailed: (data) ->
        AppDispatcher.dispatch
            type: ActionTypes.LOGIN_FAILED
            content: data
        
    userLoggedIn: (data) ->
        AppDispatcher.dispatch
            type: ActionTypes.LOGIN_SUCCESS
            content: data
        
    userLoggedOut: () ->
        AppDispatcher.dispatch
            type: ActionTypes.LOGOUT_SUCCESS
        
    checkDbSync: () ->
        AppDispatcher.dispatch
            type: ActionTypes.CHECK_SYNC
        
    syncComplete: () ->
        AppDispatcher.dispatch
            type: ActionTypes.SYNC_COMPLETE
        
