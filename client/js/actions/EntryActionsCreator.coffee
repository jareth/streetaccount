#!/usr/bin/env coffeenpm 

AppDispatcher = require '../dispatcher/AppDispatcher'
ActionTypes = require('../constants/EntryConstants').ActionTypes

module.exports = 
    savePurchases: (items, date) ->
        AppDispatcher.dispatch
            type: ActionTypes.SAVE_PURCAHSES
            content: 
                items: items
                date: date
        
    saveSales: (items, date) ->
        AppDispatcher.dispatch
            type: ActionTypes.SAVE_SALES
            content: 
                items: items
                date: date
        
    loadDateEntries: (date) ->
        AppDispatcher.dispatch
            type: ActionTypes.LOAD_DATE_ENTRIES
            content: date
        
    newEntryAdded: (date) ->
        AppDispatcher.dispatch
            type: ActionTypes.NEW_ENTRY_ADDED
            content: date
        
