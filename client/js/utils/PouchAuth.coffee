#!/usr/bin/env coffeenpm 

PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-authentication'
UserActionsCreator = require '../actions/UserActionsCreator'
require('es6-promise').polyfill()
require 'whatwg-fetch'

window.PouchDB = PouchDB
  
setupPouch = (user_id) ->
    PouchAuth.pouchdb = new PouchDB user_id

    PouchAuth.sync = PouchAuth.pouchdb.sync PouchAuth.remote_pouchdb, 
            live: true
            retry: false
        .on 'change', (info) ->
            console.log 'POUCHSYNC change', info
        .on 'paused', () ->
            console.log 'POUCHSYNC paused'
            UserActionsCreator.syncComplete()
        .on 'active', () ->
            console.log 'POUCHSYNC active'
        .on 'denied', (info) ->
            console.log 'POUCHSYNC denied', info
        .on 'complete', (info) ->
            console.log 'POUCHSYNC complete', info
        .on 'error', (err) ->
            console.log 'POUCHSYNC error', err

PouchAuth = 
    checkStatus: (response) ->
        if response.status in [200..300]
            return response
        else
            error = new Error response.statusText
            error.response = response
            throw error

    registerUser: (data) ->
        fetch '/api/register',
                credentials: 'same-origin'
                method: 'post'
                headers: 
                    'Accept': 'application/json'
                    'Content-Type': 'application/json'
                body: JSON.stringify data
            .then @checkStatus
            .then (response) ->
                return response.json()
            .then (json) =>
                if json.toast? or json.validation? 
                    UserActionsCreator.registerFailed json
                else 
                    @createSession json.user_id, data.password
            .catch (error) ->
                UserActionsCreator.registerFailed toast: "Error creating account, please contact support"
                
    loginUser: (data) ->
        fetch '/api/login',
                credentials: 'same-origin'
                method: 'post'
                headers: 
                    'Accept': 'application/json'
                    'Content-Type': 'application/json'
                body: JSON.stringify data
            .then @checkStatus
            .then (response) ->
                return response.json()
            .then (json) =>
                if json.toast? or json.validation? 
                    UserActionsCreator.loginFailed json
                if json.valid then @createSession json.user_id, data.password
            .catch (error) ->
                UserActionsCreator.loginFailed toast: "Error logging in. Check your details and try again."
        
    createSession: (user_id, password)->
        @remote_pouchdb = new PouchDB 'https://jareth.cloudant.com/' + user_id,
            skipSetup: true
        @remote_pouchdb.login user_id, password, 
                ajax: 
                    headers:
                        'Content-Type': 'application/x-www-form-urlencoded'
                        'Authorization': 'Basic ' + window.btoa(user_id + ':' + password)
            .then (response) =>
                # response = {ok: true, name: "test_user", roles: Array[0]}
                setupPouch response.name
                UserActionsCreator.userLoggedIn response.name
            .catch (error) ->
                console.log 'login failed', error
        
    logoutUser: (user_id) ->
        if @remote_pouchdb? then @remote_pouchdb.logout()
        the_pouch = if @pouchdb? then @pouchdb else new PouchDB user_id
        the_pouch.destroy()
            .then (result) ->
                UserActionsCreator.userLoggedOut()
            .catch (error) ->
                console.log 'LOGOUT ERROR', error
        
    checkDbSync: (user_id) ->
        @remote_pouchdb = new PouchDB 'https://jareth.cloudant.com/' + user_id,
            skipSetup: true
        @remote_pouchdb.getSession()
            .then (result) ->
                if result.userCtx.name?
                    setupPouch user_id  
                    UserActionsCreator.userLoggedIn user_id
                else 
                    UserActionsCreator.userLoggedOut()
            .catch (error) ->
                UserActionsCreator.userLoggedOut()
        
module.exports = PouchAuth