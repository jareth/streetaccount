day_texts = [
    'Sunday'
    'Monday'
    'Tuesday'
    'Wednesday'
    'Thursday'
    'Friday'
    'Saturday'
]

relative_day_texts = [
    'Today'
    'Yesterday'
]

month_texts = [
    'January'
    'February'
    'March'
    'April'
    'May'
    'June'
    'July'
    'August'
    'September'
    'October'
    'November'
    'December'
]

date_postfixs =
    1: 'st'
    2: 'nd'
    3: 'rd'
    21: 'st'
    22: 'nd'
    23: 'rd'
    31: 'st'

DateUtil = 
    getDayText: (date) ->
        now = new Date();
        today = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1)
        diff = today.getTime() - date.getTime()
        dayDiff = Math.floor(diff / 1000 / 60 / 60 / 24)
        return relative_day_texts[dayDiff] if dayDiff < relative_day_texts.length
        return day_texts[date.getDay()]
        
    getDateWithPostfix: (date) ->
        date.getDate().toString() + (date_postfixs[date.getDate()] or 'th')
        
    getMonthText: (date) ->
        month_texts[date.getMonth()]
        
    getNiceDateString: (date) ->
        @getDayText(date) + ', ' + @getDateWithPostfix(date) + ' ' + @getMonthText(date) + ' ' + date.getFullYear()
        
module.exports = DateUtil