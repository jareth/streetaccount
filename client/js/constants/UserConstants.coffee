#!/usr/bin/env coffeenpm 

keyMirror = require 'keymirror'

module.exports = 
    ActionTypes: keyMirror 
        REGISTER_REQUEST: null
        REGISTER_FAILED: null
        LOGIN_REQUEST: null
        LOGOUT_REQUEST: null
        LOGIN_SUCCESS: null
        LOGIN_FAILED: null
        LOGOUT_SUCCESS: null
        CHECK_SYNC: null
        SYNC_COMPLETE: null