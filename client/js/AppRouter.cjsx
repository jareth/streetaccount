# @cjsx React.DOM 

React = require 'react'
{ Router, Route, Link } = require 'react-router'

UserStore = require './stores/UserStore'

App = require './components/App'
Home = require './components/Home'
Contact = require './components/Contact'
Register = require './components/Register'
Login = require './components/Login'
Today = require './components/Today'
Purchases = require './components/Purchases'
Sales = require './components/Sales'
Analytics = require './components/Analytics'
    
requireAuth = (nextState, replaceState) ->
    if not UserStore.getIsLoggedIn()
        replaceState({ nextPathname: nextState.location.pathname }, '/login')
        
requireUnauth = (nextState, replaceState) ->
    if UserStore.getIsLoggedIn()
        replaceState({ nextPathname: nextState.location.pathname }, '/today')
        
routes = 
    path: '/',
    component: App,
    indexRoute: { component: Home }
    childRoutes: [ 
        { path: '/contact', component: Contact }
        { path: '/register', component: Register, onEnter: requireUnauth }
        { path: '/login', component: Login, onEnter: requireUnauth }
        { path: '/today(/:date)', component: Today, onEnter: requireAuth }
        { path: '/purchases(/:date)', component: Purchases, onEnter: requireAuth }
        { path: '/sales(/:date)', component: Sales, onEnter: requireAuth }
        { path: '/analytics', component: Analytics, onEnter: requireAuth }
    ]

AppRouter = React.createClass
    render: ->
        <Router routes={routes} />

module.exports = AppRouter