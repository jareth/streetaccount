# @cjsx React.DOM 

React = require 'react'
PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-upsert'

{ Col, Card, Collapsible, CollapsibleItem, Collection, CollectionItem, Button, Modal } = require 'react-materialize'

EntryFormDialog = require './EntryFormDialog'

Dashboard = React.createClass

    getInitialState: () ->
        return {
            days: []
            weekly_total: 0
            weekly_entries: 0
            new_entry: null
        }
    
    componentDidMount: () ->
        @readDatabase(@props.username) if @props.username isnt ''
    
    componentWillReceiveProps: (nextProps) ->
        @readDatabase(nextProps.username) if nextProps.username isnt ''
    
    readDatabase: (username) ->
        @pouchdb = new PouchDB username
        @pouchdb.putIfNotExists
                "_id": "_design/sales"
                "views": 
                    "byDay": 
                        "map": "function(doc) {if(doc.date){emit(doc.date, [doc.total, 1]);}}"
                        "reduce": "_sum"
                    "byItem":
                        "map": "function(doc) {if(doc.items){for(var i=0;i<doc.items.length;i++){var item=doc.items[i];emit([doc.date[0], doc.date[1], doc.date[2], item.item, i], [parseInt(item.quantity,10),item.total]);}}}"
                        "reduce": "_sum"
                "language": "javascript"
            .then (result) =>
                now = new Date()
                now.setDate(now.getDate() - now.getDay())
                console.log 'search', now.getFullYear(), now.getMonth(), now.getDate()
                @pouchdb.query 'sales/byDay',
                    startkey: [now.getFullYear(), now.getMonth(), now.getDate()]
                    group: true
                    group_level: 1
            .then (result) =>
                @setState
                    weekly_total: result.rows?[0]?.value[0] || 0
                    weekly_entries: result.rows?[0]?.value[1] || 0
                @pouchdb.query 'sales/byDay',
                    group: true
                    group_level: 3
                    descending: true
            .then (result) =>
                @setState({days: result.rows})
            .catch (error) ->
                console.log error
                
    expand: (data)->
        date = data[1]
        @setState @state
        @pouchdb.query 'sales/byItem',
                startkey: date
                endkey: date.concat {}
                group: true
                group_level: 4
            .then (result) =>
                @state.days[data[0]].sales = result.rows
                @setState @state
            .catch (error) ->
                console.log error
                
    _handleNewButton: (event) ->
        @setState new_entry: true
                
    _onDialogSubmit: (event) ->
        @setState new_entry: null
                
    _handleRequestClose: (event) ->
        @setState new_entry: null
                
    _handleSaveEntry: (event) ->
        if @refs.entryForm.save()
            @readDatabase(@props.username)
            $('#' + @refs.entryModal.id).closeModal();
            
        
    render: () -> 
        <div style={{maxWidth: 512, margin: 'auto'}}>
            <Card title='Weekly total' style={{textAlign: 'center'}}>
                <h1>${@state.weekly_total.toFixed(2)}</h1>
                From {@state.weekly_entries} sales
            </Card>
            { @renderSales() }
            
            <Button 
                    floating 
                    fab='vertical' 
                    icon='mode_edit' 
                    className='red' 
                    large 
                    style={{bottom: '45px', right: '24px'}} >
                {# @renderNewEntry() }
                <Button 
                    floating 
                    node='a'
                    href='#/entry'
                    waves='light'
                    icon='add' 
                    className='red' />
            </Button>
        </div>
        
    renderSales: () ->
        return null unless @state.days?
        
        <Collapsible>
            { for entry, index in @state.days
                <CollapsibleItem 
                        key={index}
                        eventKey={ [index, entry.key] } 
                        expanded={true}
                        onSelect={@expand}
                        header={new Date(entry.key[0], entry.key[1], entry.key[2]).toDateString() + ': $' + entry.value[0].toFixed(2) } 
                        icon='today'>
        
                    <Collection>
                        { @renderSaleItems(entry) }
                    </Collection>
                </CollapsibleItem>
            }
        </Collapsible>
        
    renderSaleItems: (entry) ->
        return null unless entry.sales?
        
        for sale, sale_index in entry.sales
            <CollectionItem key={sale.id + '_' + sale_index} active={false} style={{color: if sale.value[1] > 0 then 'green' else 'red'}}>
                {sale.key[3]} x {sale.value[0]}: ${sale.value[1].toFixed(2) }
            </CollectionItem>
        
    renderNewEntry: () ->
        <Modal
            header='New entry'
            ref='entryModal'
            style={{ 
                height: '90%'
                width: '100%'
                maxHeight: '800px'
                maxWidth: '600px'
            }}
            fixedFooter
            footer={[
                <Button key='save' waves='light' onClick={@_handleSaveEntry} flat>Save</Button>
                <Button key='close' waves='light' modal='close' flat>Cancel</Button>
                ]}
            trigger={
                <Button 
                    floating 
                    waves='light'
                    icon='add' 
                    className='red' />
            }>
            <EntryFormDialog ref='entryForm' username={@props.username}/>
        </Modal>
                
    # renderSales: ->
    #     return null unless @state.days?
        
    #     for entry, index in @state.days
    #         <li key=index>{new Date(entry.key[0], entry.key[1], entry.key[2]).toDateString()}: ${entry.value} 
    #                 {' '}<a href='#' 
    #                     data-index={index} 
    #                     data-date={JSON.stringify entry.key} 
    #                     onClick={@expand}
    #                 >v</a>
    #             { @renderSaleItems(entry) }
    #         </li>
    
    # renderSaleItems: (entry) ->
    #     return null unless entry.sales?
        
    #     <ul>
    #         { for sale, sale_index in entry.sales
    #             <li key={sale.id + '_' + sale_index}>
    #                 {sale.key[3]} x {sale.value[0]}: ${sale.value[1]}
    #             </li>
    #         }
    #     </ul>
        
    # renderNewSaleDialog: () ->
    #     <Dialog 
    #             title='Create new entry'
    #             open={@state.new_entry?} 
    #             actions={[
    #                     { text: 'Cancel' },
    #                     { text: 'Submit', onTouchTap: @_onDialogSubmit, ref: 'submit' }
    #                 ]}
    #             onRequestClose={@_handleRequestClose} >
    #         <EntryForm />
    #     </Dialog>
        
    # render: ->
    #     <div>
    #         <Card>
    #             <CardTitle>Weekly total:</CardTitle>
    #             <CardText>
    #                 <p>${@state.weekly_total}</p>
    #                 <p>From {@state.weekly_entries} sales</p>
    #             </CardText>
    #         </Card> 
    #         <Card>
    #             <ul>
    #                 { @renderSales() }
    #             </ul>
    #         </Card>
    #         <div style={{position: 'fixed', bottom: '24px', right: '24px'}}>
    #             <FloatingActionButton onTouchTap={@_handleNewButton} >
    #                 <FontIcon className="material-icons">add</FontIcon>
    #             </FloatingActionButton>
    #         </div>
    #         {@renderNewSaleDialog() }
    #     </div>


            # <Button 
            #         floating 
            #         fab='vertical' 
            #         icon='mode_edit' 
            #         className='red' 
            #         large 
            #         style={{bottom: '45px', right: '24px'}} >
            #     {# @renderNewEntry() }
            # </Button>

module.exports = Dashboard