# @cjsx React.DOM 

React = require 'react'
{ History } = require 'react-router'

DateUtil = require '../utils/DateUtil'

EntryStore = require '../stores/EntryStore'
EntryActionsCreator = require '../actions/EntryActionsCreator'

TemplateStore = require '../stores/TemplateStore'
TemplateActionsCreator = require '../actions/TemplateActionsCreator'

{ Row, Col, Card, Input, Button } = require 'react-materialize'

Purchases = React.createClass
    
    mixins: [ History ]

    getInitialState: () ->
        return {
            items: @populateTemplateItems()
            total: 0
            saving: false
        }
    
    componentDidMount: () ->
        EntryStore.changeSignal.add @onEntryStoreChanged
        TemplateStore.changeSignal.add @onTemplateChanged
        
    componentWillUnmount: () ->
        EntryStore.changeSignal.remove @onEntryStoreChanged
        TemplateStore.changeSignal.remove @onTemplateChanged
        
    onEntryStoreChanged: () ->
        if @state.saving
            TemplateActionsCreator.savePurchasesTemplate @state.items
            @history.pushState null, '/today/' + @props.params.date
            
    onTemplateChanged: () ->
        if @state.items.length <= 1
            @setState items: @populateTemplateItems()
            
    populateTemplateItems: () ->
        template = TemplateStore.getPurchasesTemplate()
        console.log template
        items = []
        for item in template
            items.push 
                item: item.item
                unit: item.unit
                total: 0
        items.push
            total: 0
        return items
        
    handleSave: (event)->
        event.preventDefault()
        @setState saving: true
        EntryActionsCreator.savePurchases @state.items, @props.params.date
                
    updateTotal: () ->
        total = 0.0
        for item in @state.items
            total += parseFloat item.total
        @setState total: total
        
    handleChange: (event)->
        item = @state.items[event.target.dataset.index]
        item[event.target.name] = event.target.value
        @updateTotal()
        
    handleAddItem: (event) ->
        event.preventDefault()
        items = @state.items.concat
            quantity: 0
            total: 0
        @setState items: items
                    
    
    render: ->
        <Row>
            <Col s={12}>
                <Card 
                    title={DateUtil.getNiceDateString(new Date(parseInt(@props.params.date)))}/>
            </Col>
            <Col s={12}>
                <Card 
                    title='Purchases'
                    node='form'
                    id='new' 
                    onSubmit={@handleNew}>
                    { @renderHeader() }
                    { @renderRows() }
                    <Row>
                        <Col s={10}>
                            <Button 
                                node='a' 
                                href='#'
                                waves='light'
                                onClick={@handleAddItem}>
                                    Add Material
                                </Button>
                        </Col>
                        <Col s={2}>
                            {'$' + @state.total.toFixed(2) }
                        </Col>
                    </Row>
                    <Row>
                        <Col s={3} offset='s9'>
                            <Button 
                                node='a' 
                                href='#/today'
                                waves='light'
                                onClick={@handleSave}>
                                    Save
                                </Button>
                        </Col>
                    </Row>
                </Card>
            </Col>
        </Row>
    
    renderHeader: () ->
        return null unless @state.items?
          
        <Row>
            <Col s={6}>Material I Buy
            </Col>
            <Col s={2}>Unit
            </Col>
            <Col s={2}>Quantity
            </Col>
            <Col s={2}>Total
            </Col>
        </Row>
    
    renderRows: () ->
        return null unless @state.items?
        
        for item, index in @state.items
            <Row key={index}>
                <Input 
                        s={6} 
                        type='text'
                        name='item'
                        data-index={index}
                        value={@state.items[index].item}
                        onChange={@handleChange} />
                <Input 
                        s={2} 
                        type='text' 
                        name='unit'
                        data-index={index}
                        value={@state.items[index].unit}
                        onChange={@handleChange} />
                <Input 
                        s={2} 
                        type='number' 
                        step={0.01}
                        name='quantity'
                        data-index={index}
                        value={@state.items[index].quantity}
                        onChange={@handleChange} />
                <Input 
                        s={2} 
                        type='number'
                        step={0.01}
                        ref={'total_' + index}
                        name='total'
                        data-index={index}
                        value={@state.items[index].total}
                        onChange={@handleChange} />
            </Row>

module.exports = Purchases