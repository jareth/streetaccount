# @cjsx React.DOM 

React = require 'react'
{ History } = require 'react-router'

{ Footer, Modal } = require 'react-materialize'

UserStore = require '../stores/UserStore'
UserActionsCreator = require '../actions/UserActionsCreator'
Nav = require './Nav'

App = React.createClass
    
    mixins: [ History ]

    getInitialState: () ->
        logged_in: false
        username: ''

    componentDidMount: () ->
        UserStore.changeSignal.add @onUserStoreChanged
        UserActionsCreator.checkDbSync()
        @setState
            logged_in: UserStore.getIsLoggedIn()
            username: UserStore.getUsername()
        
    onUserStoreChanged: () ->
        wasLoggedIn = @state.logged_in
        @setState
            logged_in: UserStore.getIsLoggedIn()
            username: UserStore.getUsername()
        if UserStore.getIsLoggedIn() and not wasLoggedIn then @history.pushState null, '/today'
        if not UserStore.getIsLoggedIn() and wasLoggedIn then @history.pushState null, '/login'
        
    render: ->
        <div id='page-container'>
            <header>
                <Nav logged_in={@state.logged_in} />
            </header>
            { if @props.location.pathname is '/' then @renderContainedChildren() else @renderContainedChildren() }
            <Footer 
                    className='orange'
                    copyrights="© 2015 akkounta.com"
                    moreLinks={
                        <Modal
                                header='Terms & Conditions'
                                trigger={<a className='right white-text'>Terms & Conditions</a>}>
                            <p>
                                Please note that this is a beta version of the akkounta website which is
                                still undergoing intensive testing and development before its release. The
                                website, its software and all content found on it are provided on an
                                “as is” and “as available” basis. akkounta does not give any warranties,
                                whether express or implied, as to the suitability or usability of the
                                website, its software or any of its content.
                            </p>
                            <p>
                                akkounta will not be liable for any loss, whether such loss is direct,
                                indirect, special or consequential, suffered by any party as a result
                                of their use of the akkounta website, its software or content. 
                            </p>
                            <p>
                                Any uploading of information to the website is done at the
                                user’s own risk and the user will be solely responsible for any
                                damage to any computer system or loss of data that results from such
                                activities. akkounta in the current beta Version does not substitute 
                                your own bookkeeping and your own storing of your business, sales and other 
                                information!
                            </p>
                            <p>
                                Should you encounter any bugs, glitches, lack of functionality or
                                other problems on the website, please let us know immediately so we
                                can rectify these accordingly. Your help in this regard is greatly
                                appreciated.
                            </p>
                        </Modal>
                    }>
                <h5 className='white-text'>Beta Version</h5>
                <p className='white-text'>
                    Thank you for trying the beta version of akkounta.
                </p>
                <p className='white-text'>
                    If you have any issues, suggestions or feedback please contact us at: 
                    <a className='white-text' href='mailto:akkountabeta@gmail.com'>akkountabeta@gmail.com</a>
                </p>
            </Footer>
        </div>
        
    renderContainedChildren: () ->
        <main className='container'>
            { React.cloneElement(@props.children, {username: @state.username}) if @props.children? }
        </main>
        
    renderRawChildren: () ->
        React.cloneElement(@props.children, {username: @state.username}) if @props.children?

module.exports = App