# @cjsx React.DOM 

React = require 'react'
UserActionsCreator = require '../actions/UserActionsCreator'

{ Navbar, NavItem, Icon } = require('react-materialize')

Nav = React.createClass
        
    _handleLogout: (event) ->
        event.preventDefault()
        UserActionsCreator.logoutUser()
    
    renderAuthNavItems: () ->
        [
            <NavItem key='today' href='#/today' >Today<Icon left>today</Icon></NavItem>
            <NavItem key='analytics' href='#/analytics' >Analytics<Icon left>timeline</Icon></NavItem>
            <NavItem key='logout' href='#' onClick={@_handleLogout} >Logout<Icon left>power_settings_new</Icon></NavItem>
        ]
    
    renderOpenNavItems: () ->
        [
            <NavItem key='login' href='#/login' >Login<Icon left>power_settings_new</Icon></NavItem>
            <NavItem key='register' href='#/register' >Register<Icon left>person_add</Icon></NavItem>
        ]
        
    render: ->
        <nav>
            <Navbar brand='akkounta' right className='orange'>
                <NavItem href='#/'>Home<Icon left>home</Icon></NavItem>
                { if @props?.logged_in then @renderAuthNavItems() else @renderOpenNavItems() }
                
            </Navbar>
        </nav>

module.exports = Nav
#<NavItem href='#/contact'>Contact<Icon left>contact_mail</Icon></NavItem>