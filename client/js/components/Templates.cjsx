# @cjsx React.DOM 

React = require 'react'
PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-find'

{ Row, Col, Card, Input, Button } = require 'react-materialize'

Templates = React.createClass

    getInitialState: () ->
        return {}
    
    componentDidMount: () ->
        @readDatabase(@props.username) if @props.username isnt ''
    
    componentWillReceiveProps: (nextProps) ->
        @readDatabase(nextProps.username) if nextProps.username isnt ''
    
    readDatabase: (username) ->
        @pouchdb = new PouchDB username
        console.log @pouchdb
        @pouchdb.createIndex
                index:
                    fields: ['type']
            .then (result) =>
                @pouchdb.find
                    selector: 
                        type: 'item'
            .then (result) =>
                @setState({data: result.docs})
            .catch (error) ->
                console.log error
                
    handleUpdate: (event)->
        event.preventDefault()
        doc = @state.data[event.target.id]
        doc[element.name] = element.value for element in event.target.elements
        @pouchdb.put doc
            .then (result) ->
                console.log 'success', result
            .catch (err) ->
                console.log 'error', err
            
    handleNew: (event)->
        event.preventDefault()
        doc =
            _id: 'item_' + new Date().getTime()
            type: 'item'
        doc[element.name] = element.value for element in event.target.elements when element.name isnt ''
        @pouchdb.put doc
            .then (result) =>
                console.log 'success', result
                doc._rev = result.rev
                @state.data.push doc
                @setState @state
                element.value = '' for element in event.target.elements when element.name isnt ''
            .catch (err) ->
                console.log 'error', err
        
    handleChange: (event)->
        @state.data[event.target.form.id][event.target.name] = event.target.value
        @setState(@state)
        
    delete: (event)->
        event.preventDefault()
        id = event.target.parentElement.dataset.id
        doc =
            _id: @state.data[id]._id
            _rev: @state.data[id]._rev
        @pouchdb.remove doc
            .then (result) =>
                @state.data.splice id, 1
                @setState @state
            .catch (error) ->
                console.log 'delete error', error
    
    render: ->
        renderedEntries = @state.data?.map (entry, index) =>
            <Row node='form' key={entry._id} id={index} onSubmit={@handleUpdate} >
                <Input 
                    s={6}
                    type='text' 
                    name='name' 
                    value={entry.name} 
                    onChange={@handleChange} />
                <Input 
                    s={4}
                    type='number' 
                    name='cost' 
                    step='0.01' 
                    value={entry.cost} 
                    onChange={@handleChange} />
                <Button floating s={1} icon='done' />
                <Button floating s={1} href='#' data-id={index} onClick={@delete} icon='delete' />
            </Row>
        <Card title='Template Items:' style={{maxWidth: 512, margin: 'auto'}}>
            <Row>
                <Col s={6}>Item
                </Col>
                <Col s={4}>Cost
                </Col>
                <Col s={2}>Actions
                </Col>
            </Row>
            { renderedEntries }
            <Row node='form' id='new' onSubmit={@handleNew} >
                <Input 
                    s={6}
                    type='text' 
                    name='name' 
                    placeholder='name' />
                <Input 
                    s={4}
                    type='number' 
                    step='0.01'
                    name='cost' 
                    placeholder='cost' />
                <Button floating s={2} icon='add' />
            </Row>
        </Card>

module.exports = Templates