# @cjsx React.DOM 

React = require 'react'
{ CardPanel, Row, Col } = require 'react-materialize'

Home = React.createClass
        
    kickstartParallax: (component) ->
        $(component).parallax()
        
    render: ->
        <Row>
            <Col s={12}>
                <CardPanel>
                    <p>
                        Welcome friend! Are you running a small business and want to 
                        better control how much money you invest in raw materials and how 
                        much money you really earn selling your product to your customers? 
                        akkounta helps you to manage all this and shows you what is 
                        your real win (or loss).
                    </p>
                    <p>
                        This is still a beta version and you can use it for free as long 
                        as you like. In exchange, help us to make akkounta better by
                        providing us with your detailed feedback under the Contact item.
                        We will consider all your suggestions and greatly appreciate your 
                        support!
                    </p>
                    <p>Cheers,</p>
                    <p>your akkounta team.</p>
                </CardPanel>
            </Col>
        </Row>

module.exports = Home
