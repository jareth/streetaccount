# @cjsx React.DOM 

React = require 'react'
validate = require 'validate.js'

UserStore = require '../stores/UserStore'
UserActionsCreator = require '../actions/UserActionsCreator'
LoginValidations = require('../validations/UserValidations').LoginValidations

{ Row, Col, Card, CardPanel, Input, Button } = require 'react-materialize'

Login = React.createClass

    getInitialState: ->
        return {
            email: ''
            password: ''
            validation: null
        }
    
    componentDidMount: () ->
        UserStore.changeSignal.add @onUserStoreChanged
        @readStoreValues()
        
    componentWillUnmount: () ->
        UserStore.changeSignal.remove @onUserStoreChanged
    
    onUserStoreChanged: () ->
        @readStoreValues()
        
    readStoreValues: () ->
        login_errors = UserStore.getLoginErrors()
        if login_errors.toast? then Materialize.toast login_errors.toast, 4000
        @setState validation: login_errors.validation
            
    handleChange: (event)->
        newValues = {}
        newValues[event.target.name] = event.target.value
        this.setState newValues
            
    handleSubmit: (event)->
        event.preventDefault()
        validations = validate(@state, LoginValidations)
        @setState validation: validations
        if not validations? then UserActionsCreator.loginUser this.state
        
    render: ->
        <Row node='form' onSubmit={@handleSubmit}>
            <Col offset='m2 l3' l={6} m={8} s={12} >
                <Card 
                        title='Login to akkounta'
                        actions={[
                            <Button 
                                key='submit' 
                                node='button' 
                                waves='light'>
                                Login
                            </Button>]}>
                    <Row>
                        <Input 
                            s={12}
                            type='text'
                            name='email'
                            label='Email'
                            value={@state.email} 
                            errorLabel={@state.validation?.email?[0]}
                            className={if @state.validation?.email? then 'invalid'}
                            onChange={@handleChange} />
                        <Input 
                            s={12}
                            type='password' 
                            name='password' 
                            label='Password'
                            value={@state.password} 
                            errorLabel={@state.validation?.password?[0]}
                            className={if @state.validation?.password? then 'invalid'}
                            onChange={@handleChange}/>
                    </Row>
                </Card>
            </Col>
        </Row>

module.exports = Login