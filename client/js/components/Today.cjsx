# @cjsx React.DOM 

React = require 'react'

DateUtil = require '../utils/DateUtil'
EntryStore = require '../stores/EntryStore'
EntryActionsCreator = require '../actions/EntryActionsCreator'

{ Row, Col, CardPanel, CardTitle, Button } = require 'react-materialize'

generateKeyString = (date, type) ->
    [
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        type
    ].join ','
    
isToday = (date) ->
    now = new Date()
    today = new Date(now.getFullYear(), now.getMonth(), now.getDate())
    return date.getTime() > today.getTime()

Today = React.createClass

    getInitialState: () ->
        date = new Date()
        date.setDate(date.getDate())
        return {
            date: date
            purchases: 0
            sales: 0
            total: 0
        }
    
    componentDidMount: () ->
        if @props.params.date?
            date = new Date(parseInt(@props.params.date))
        else 
            date = new Date()
        @setState date: date
        EntryStore.changeSignal.add @onEntryStoreChanged
        EntryActionsCreator.loadDateEntries date
        
    componentWillUnmount: () ->
        EntryStore.changeSignal.remove @onEntryStoreChanged
    
    onEntryStoreChanged: () ->
        @readStoreValues(@state.date)
        
    readStoreValues: (date) ->
        storePurchases = EntryStore.getPurchases()
        keyString = generateKeyString date, 'purchase'
        purchases = storePurchases[keyString] or 0
        
        storeSales = EntryStore.getSales()
        keyString = generateKeyString date, 'sale'
        sales = storeSales[keyString] or 0
        
        @setState
            purchases: purchases
            sales: sales
            total: sales - purchases
        
    changeDate: (event) ->
        event.preventDefault()
        amount = if event.currentTarget.dataset.direction is 'forward' then 1 else -1
        if amount > 0 and isToday @state.date
            return
        else 
            date = @state.date
            date.setDate date.getDate() + amount
            @setState date: date
            EntryActionsCreator.loadDateEntries date
    
    render: () -> 
        <Row>
            <Col l={6} m={8} s={12}>
                <CardPanel className='center-align'>
                    <Button 
                        node='a' 
                        className='btn-flat left' 
                        waves='teal' 
                        icon='arrow_back' 
                        data-direction='back'
                        onClick={@changeDate} />
                    <h4 style={{display: 'inline'}}>{DateUtil.getDayText(@state.date)}</h4>
                    <Button 
                        node='a' 
                        style={ if isToday @state.date then {visibility:'hidden'} }
                        className='btn-flat right'
                        waves='teal' 
                        icon='arrow_forward' 
                        data-direction='forward' 
                        onClick={@changeDate} />
                    <h1>{DateUtil.getDateWithPostfix(@state.date)}</h1>
                    <h5>{DateUtil.getMonthText(@state.date)}, {@state.date.getFullYear()}</h5>
                </CardPanel>
            </Col>
            
            <Col l={3} m={4} s={6}>
                <CardPanel className='center-align'>
                    <h5>Purchases ${@state.purchases.toFixed(2)}</h5>
                    <Button 
                        floating 
                        large 
                        node='a'
                        waves='light'
                        className='teal' 
                        icon='add' 
                        href={'#/purchases/' + @state.date.getTime()} />
                </CardPanel>
            </Col>
            
            <Col l={3} m={4} s={6} >
                <CardPanel className='center-align'>
                    <h5>Sales ${@state.sales.toFixed(2)}</h5>
                    <Button 
                        floating 
                        large
                        node='a'
                        waves='light'
                        className='teal' 
                        icon='add'
                        href={'#/sales/' + @state.date.getTime()} />
                </CardPanel>
            </Col>
            <Col l={6} s={12} >
                <CardPanel className={if @state.total < 0 then 'red-text' else 'green-text'}>
                    <h1>${Math.abs(@state.total).toFixed(2)}</h1>
                    <h3>{if @state.total < 0 then 'Loss' else 'Profit'}</h3>
                </CardPanel>
            </Col>
        </Row>

module.exports = Today