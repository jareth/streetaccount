# @cjsx React.DOM 

React = require 'react'
{ History } = require 'react-router'

DateUtil = require '../utils/DateUtil'

EntryStore = require '../stores/EntryStore'
EntryActionsCreator = require '../actions/EntryActionsCreator'

TemplateStore = require '../stores/TemplateStore'
TemplateActionsCreator = require '../actions/TemplateActionsCreator'

{ Row, Col, Card, Input, Button } = require 'react-materialize'

Sales = React.createClass
    
    mixins: [ History ]

    getInitialState: () ->
        return {
            items: @populateTemplateItems()
            total: 0
            saving: false
        }
    
    componentDidMount: () ->
        EntryStore.changeSignal.add @onEntryStoreChanged
        TemplateStore.changeSignal.add @onTemplateChanged
        
    componentWillUnmount: () ->
        EntryStore.changeSignal.remove @onEntryStoreChanged
        TemplateStore.changeSignal.remove @onTemplateChanged
        
    onEntryStoreChanged: () ->
        if @state.saving
            TemplateActionsCreator.saveSalesTemplate @state.items
            @history.pushState null, '/today/' + @props.params.date
        
    onTemplateChanged: () ->
        if @state.items.length <= 1
            @setState items: @populateTemplateItems()
            
    populateTemplateItems: () ->
        template = TemplateStore.getSalesTemplate()
        items = []
        for item in template
            items.push 
                item: item.item
                price: item.price
                quantity: 0
                total: 0
        items.push
            price: 0
            quantity: 0
            total: 0
        return items
        
    handleSave: (event)->
        event.preventDefault()
        @setState saving: true
        EntryActionsCreator.saveSales @state.items, @props.params.date
                
    updateTotal: () ->
        total = 0.0
        for item in @state.items
            total += item.total
        @setState total: total
        
    handleChange: (event)->
        item = @state.items[event.target.dataset.index]
        item[event.target.name] = event.target.value
        item.total = parseFloat item.price * parseFloat item.quantity
        @updateTotal()
        
    handleAddItem: (event) ->
        event.preventDefault()
        items = @state.items.concat 
            price: 0
            quantity: 0
            total: 0
        @setState items: items
    
    render: ->
        <Row>
            <Col s={12}>
                <Card 
                    title={DateUtil.getNiceDateString(new Date(parseInt(@props.params.date)))}/>
            </Col>
            <Col s={12}>
                <Card 
                    title='Sales'
                    node='form'
                    id='new' 
                    onSubmit={@handleNew}>
                    { @renderHeader() }
                    { @renderRows() }
                    <Row>
                        <Col s={10}>
                            <Button 
                                node='a' 
                                href='#'
                                waves='light'
                                onClick={@handleAddItem}>
                                    Add Product
                                </Button>
                        </Col>
                        <Col s={2}>
                            {'$' + @state.total.toFixed(2) }
                        </Col>
                    </Row>
                    <Row>
                        <Col s={3} offset='s9'>
                            <Button 
                                node='a' 
                                href='#/today'
                                waves='light'
                                onClick={@handleSave}>
                                    Save
                                </Button>
                        </Col>
                    </Row>
                </Card>
            </Col>
        </Row>
    
    renderHeader: () ->
        return null unless @state.items?
          
        <Row>
            <Col s={6}>My Product
            </Col>
            <Col s={2}>Price
            </Col>
            <Col s={2}># Sold
            </Col>
            <Col s={2}>Total
            </Col>
        </Row>
    
    renderRows: () ->
        return null unless @state.items?
        
        for item, index in @state.items
            <Row key={index}>
                <Input 
                        s={6} 
                        type='text'
                        name='item'
                        data-index={index}
                        value={@state.items[index].item}
                        onChange={@handleChange} />
                <Input 
                        s={2} 
                        type='number'
                        step=0.01
                        name='price'
                        data-index={index}
                        value={@state.items[index].price}
                        onChange={@handleChange} />
                <Input 
                        s={2} 
                        type='number' 
                        name='quantity'
                        data-index={index}
                        value={@state.items[index].quantity}
                        onChange={@handleChange} />
                <Input 
                        s={2} 
                        ref={'total_' + index}
                        name='total'
                        step={0.01}
                        disabled
                        value={'$' + @state.items[index].total.toFixed(2)}
                        data-index={index} />
            </Row>
            
module.exports = Sales