# @cjsx React.DOM 

React = require 'react'
{ CardPanel } = require 'react-materialize'

Home = React.createClass
        
    kickstartParallax: (component) ->
        $(component).parallax()
        
    render: ->
        <div className=''>
            <div className='parallax-container' style={{height: 400}}>
                <div ref={@kickstartParallax} className='parallax'>
                    <img src='img/spoon.jpg' />
                </div>
            </div>
            <div className='section white z-depth-1' style={{padding: 20}}>
                <div className='container'>
                    <p>
                        Welcome friend! Are you running a small business and want to 
                        better control how much money you invest in raw materials and how 
                        much money you really earn selling your product to your customers? 
                        akkounta helps you to manage all this and shows you what is 
                        your real win (or loss).
                    </p>
                    <p>
                        This is still a beta version and you can use it for free as long 
                        as you like. In exchange, help us to make akkounta better by
                        providing us with your detailed feedback under the Contact item.
                        We will consider all your suggestions and greatly appreciate your 
                        support!
                    </p>
                    <p>Cheers,</p>
                    <p>your akkounta team.</p>
                </div>
            </div>
            <div className='parallax-container' style={{height: 400}}>
                <div ref={@kickstartParallax} className='parallax'>
                    <img src='img/carts2.jpg' />
                </div>
            </div>
        </div>

module.exports = Home
