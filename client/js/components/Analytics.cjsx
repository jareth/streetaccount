# @cjsx React.DOM 

React = require 'react'
ChartistGraph = require('react-chartist')

DateUtil = require '../utils/DateUtil'
AnalyticsStore = require '../stores/AnalyticsStore'

{ Row, Col, CardPanel, Card, Button, Chip, Icon } = require 'react-materialize'

simpleLineChartData = {
  labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
  series: [
    [12, 9, 7, 8, 5],
    [2, 1, 3.5, 7, 3],
    [1, 3, 4, 5, 6]
  ]
}

generateKeyString = (date, type) ->
    [
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        type
    ].join ','
    
isToday = (date) ->
    now = new Date()
    today = new Date(now.getFullYear(), now.getMonth(), now.getDate())
    return date.getTime() > today.getTime()

Analytics = React.createClass

    getInitialState: () ->
        return {
            week_total: 0
            month_total: 0
            has_trends: false
            has_quantities: false
        }
    
    componentDidMount: () ->
        AnalyticsStore.changeSignal.add @onAnalyticsStoreChanged
        @readStoreValues()
        
    componentWillUnmount: () ->
        AnalyticsStore.changeSignal.remove @onAnalyticsStoreChanged
    
    onAnalyticsStoreChanged: () ->
        @readStoreValues()
        
    readStoreValues: () ->
        totals = AnalyticsStore.getTotals()
        trendsAvailable = AnalyticsStore.getTrends()?
        quantitesAvailable = AnalyticsStore.getTrends()?
        
        @setState
            week_total: totals.week
            month_total: totals.month
            has_trends: trendsAvailable
            has_quantities: quantitesAvailable
    
    render: () -> 
        <Row>
            <Col l={6} s={12} >
                <CardPanel className={if @state.week_total < 0 then 'red-text' else 'green-text'}>
                    <h1>${Math.abs(@state.week_total).toFixed(2)}</h1>
                    <h3>Weekly {if @state.week_total < 0 then 'Loss' else 'Profit'}</h3>
                </CardPanel>
            </Col>
            <Col l={6} s={12} >
                <CardPanel className={if @state.month_total < 0 then 'red-text' else 'green-text'}>
                    <h1>${Math.abs(@state.month_total).toFixed(2)}</h1>
                    <h3>Monthly {if @state.month_total < 0 then 'Loss' else 'Profit'}</h3>
                </CardPanel>
            </Col>
            { @renderTrends() if @state.has_trends }
            { @renderQuantities() if @state.has_quantities }
        </Row>
        
    renderTrends: () ->
        <Col l={6} s={12} >
            <Card className='trend' title='Profit / Loss Trend'>
                <ChartistGraph data={AnalyticsStore.getTrends()} type={'Line'} />
                <div className='legend valign-wrapper'>
                    <span className='valign-wrapper'>
                        <Icon className='ct-series-a'>lens</Icon>
                        Purchases
                    </span>
                    <span className='valign-wrapper'>
                        <Icon className='ct-series-b'>lens</Icon>
                        Sales
                    </span>
                    <span className='valign-wrapper'>
                        <Icon className='ct-series-c'>lens</Icon>
                        Profit / Loss
                    </span>
                </div>
            </Card>
        </Col>
    renderQuantities: () ->
        <Col l={6} s={12} >
            <Card title='Sales Trends'>
                <ChartistGraph data={AnalyticsStore.getQuantities()} type={'Line'} />
                <div className='legend valign-wrapper'>
                    {@renderQuantitiesLegend()}
                </div>
            </Card>
        </Col>
        
    renderQuantitiesLegend: () ->
        for item, index in AnalyticsStore.getQuantities().items
            <span key={index} className='valign-wrapper'>
                <Icon className={'ct-series-' + String.fromCharCode(97 + index)}>lens</Icon>
                {item}
            </span>

module.exports = Analytics