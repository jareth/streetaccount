# @cjsx React.DOM 

React = require 'react'
PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-find'

Sales = React.createClass

    getInitialState: () ->
        return {
            items: []
            sales: []
            new_sale: 
                items: []
                total: 0
        }
    
    componentDidMount: () ->
        @readDatabase(@props.username) if @props.username isnt ''
    
    componentWillReceiveProps: (nextProps) ->
        @readDatabase(nextProps.username) if nextProps.username isnt ''
    
    readDatabase: (username) ->
        @pouchdb = new PouchDB username
        console.log @pouchdb
        @pouchdb.createIndex
                index:
                    fields: ['type']
            .then (result) =>
                @pouchdb.find
                    selector: 
                        type: 'item'
            .then (result) =>
                @setState({items: result.docs})
            .then (result) =>
                @pouchdb.find
                    selector: 
                        type: 'sale_item'
            .then (result) =>
                @setState({sale_items: result.docs})
            .then (result) =>
                @pouchdb.find
                    selector: 
                        type: 'sale'
            .then (result) =>
                @setState({sales: result.docs})
            .catch (error) ->
                console.log error
                
    handleUpdate: (event)->
        event.preventDefault()
        # doc = @state.items[event.target.id]
        # doc[element.name] = element.value for element in event.target.elements
        # @pouchdb.put doc
        #     .then (result) ->
        #         console.log 'success', result
        #     .catch (err) ->
        #         console.log 'error', err
            
    handleNew: (event)->
        event.preventDefault()
        timestamp = new Date()
        sale_id = 'sale_' + timestamp.getTime()
        total = 0
        doc =
            _id: sale_id
            type: 'sale'
            date: [
                timestamp.getFullYear(), 
                timestamp.getMonth(), 
                timestamp.getDate()
            ]
            items: []
        for item, index in @state.new_sale.items when item.item isnt '' and item.quantity isnt 0
            total += item.total
            doc.items.push item
        doc.total = total
        @pouchdb.put doc
            .then (result) =>
                console.log 'success', result
                doc._rev = result.rev
                @state.sales.push doc
                @state.new_sale =
                    items: []
                    total: 0
                @setState @state
            .catch (err) ->
                console.log 'error', err
            
        
    handleChange: (event)->
        item = @state.new_sale.items[event.target.dataset.index]
        item[event.target.name] = event.target.value  
        if event.target.selectedOptions?[0]?.dataset?.cost?
            item.cost = event.target.selectedOptions?[0]?.dataset?.cost
        item.total = item.cost * item.quantity
        @setState(@state)
        
    handleAddItem: (event) ->
        event.preventDefault()
        @state.new_sale.items.push 
            item: ''
            cost: 0
            quantity: 0
            total: 0
        @setState @state
        
        
    delete: (event)->
        event.preventDefault()
        doc =
            _id: event.target.dataset.id
            _rev: event.target.dataset.rev
        @pouchdb.remove doc
            .then (result) =>
                @state.sales.splice event.target.dataset.id, 1
                @setState @state
            .catch (error) ->
                console.log 'delete error', error
    
    render: ->
        renderedSales = @state.sales?.map (entry, index) =>
            <div key={entry._id}>
                <p>Sale: {entry._id} total: ${entry.total} on: {entry.date.toString()}
                    <a href='#' data-id={entry._id} data-rev={entry._rev} data-index={index} onClick={@delete} >Delete!</a>
                </p>
                {<p key={item_index}>Sale item: {item.item} X {item.quantity} is ${item.total}</p> for item, item_index in entry.items}
            </div>
        renderedItems = @state.new_sale.items.map (entry, index) =>
            <div key={'item_' + index}>
                <select name='item' 
                    data-index={index} 
                    onChange={@handleChange} 
                    selected='' >
                    <option value='' data-cost=0 >Select Item:</option>
                    {<option value={entry.name} data-cost={entry.cost} key={entry._id}>{entry.name}</option> for entry in @state.items }
                </select>
                <input type='number' name='quantity' data-index={index} placeholder='quantity' onChange={this.handleChange} />
            </div>
        <div>
            Sales:
            { renderedSales }
            <p>Create new sale</p>
            <form id='new' onSubmit={@handleNew} >
                { renderedItems }
                <div><a href='#' onClick={@handleAddItem}>Add Item</a></div>
                <button>Commit Sale</button>
            </form>
        </div>

module.exports = Sales