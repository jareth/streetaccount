# @cjsx React.DOM 

React = require 'react'
validate = require 'validate.js'

UserStore = require '../stores/UserStore'
UserActionsCreator = require '../actions/UserActionsCreator'
RegisterValidations = require('../validations/UserValidations').RegisterValidations

{ Row, Col, Card, Input, Button, Modal } = require 'react-materialize'

Register = React.createClass
    
    getInitialState: ->
        return {
            name: ''
            country: ''
            city: ''
            email: ''
            password: ''
            confirm_password: ''
            business: ''
            agree: false
            validation: null
        }
    
    componentDidMount: () ->
        UserStore.changeSignal.add @onUserStoreChanged
        @readStoreValues()
        
    componentWillUnmount: () ->
        UserStore.changeSignal.remove @onUserStoreChanged
    
    onUserStoreChanged: () ->
        @readStoreValues()
        
    readStoreValues: () ->
        register_errors = UserStore.getRegisterErrors()
        if register_errors.toast? then Materialize.toast register_errors.toast, 4000
        @setState validation: register_errors.validation
            
    handleChange: (event)->
        newValues = {}
        if event.target.name is 'agree'
            newValues[event.target.name] = not @state.agree
        else
            newValues[event.target.name] = event.target.value
        @setState newValues
            
    handleSubmit: (event)->
        event.preventDefault()
        validations = validate(@state, RegisterValidations)
        @setState validation: validations
        if not validations? then UserActionsCreator.registerUser @state
        
    render: ->
        <Row node='form' onSubmit={@handleSubmit}>
            <Col offset='m2 l3' l={6} m={8} s={12} >
                <Card
                    title='Register with akkounta'
                    actions={[
                        <Button 
                            key='submit'
                            node='button'
                            waves='light'>
                            Register
                        </Button>]}>
                    <Row>
                        <Input 
                            s={12}
                            label='Name' 
                            type='text' 
                            name='name' 
                            value={@state.name} 
                            errorLabel={@state.validation?.name?[0]}
                            className={if @state.validation?.name? then 'invalid'}
                            onChange={@handleChange} />
                        
                        <Input 
                                s={12}
                                label='Country'
                                type='select' 
                                name='country' 
                                errorLabel={@state.validation?.country?[0]}
                                className={if @state.validation?.country? then 'invalid'}
                                onChange={@handleChange} 
                                selected={@state.country} >
                            <option value=''></option>
                            <option value='germany' >Germany</option>
                            <option value='india' >India</option>
                            <option value='mexico' >Mexico</option>
                        </Input>
                        
                        <Input 
                            s={12}
                            label='City' 
                            type='text' 
                            name='city' 
                            value={@state.city} 
                            errorLabel={@state.validation?.city?[0]}
                            className={if @state.validation?.city? then 'invalid'}
                            onChange={@handleChange} />
                        
                        <Input 
                            s={12}
                            label='Email'
                            type='text' 
                            name='email' 
                            value={@state.email} 
                            errorLabel={@state.validation?.email?[0]}
                            className={if @state.validation?.email? then 'invalid'}
                            onChange={@handleChange} />
                        
                        <Input 
                            s={12}
                            label='Password'
                            type='password' 
                            name='password' 
                            value={@state.password} 
                            errorLabel={@state.validation?.password?[0]}
                            className={if @state.validation?.password? then 'invalid'}
                            onChange={@handleChange} />
                            
                        <Input 
                            s={12}
                            label='Confirm Password'
                            type='password' 
                            name='confirm_password' 
                            value={@state.confirm_password} 
                            errorLabel={@state.validation?.confirm_password?[0]}
                            className={if @state.validation?.confirm_password? then 'invalid'}
                            onChange={@handleChange} />
                        
                        <Input 
                                s={12}
                                label='Business'
                                type='select' 
                                name='business' 
                                errorLabel={@state.validation?.business?[0]}
                                className={if @state.validation?.business? then 'invalid'}
                                onChange={@handleChange} 
                                selected={@state.business} >
                            <option value='' ></option>
                            <option value='food' >Food</option>
                            <option value='sales' >Sales</option>
                            <option value='service' >Service</option>
                        </Input>
                        <Col s={12}>
                            <span>Please agree to the </span>
                            <Modal
                                header='Terms & Conditions'
                                trigger={<a>terms & conditions</a>}>
                                <p>
                                    Please note that this is a beta version of the akkounta website which is
                                    still undergoing intensive testing and development before its release. The
                                    website, its software and all content found on it are provided on an
                                    “as is” and “as available” basis. akkounta does not give any warranties,
                                    whether express or implied, as to the suitability or usability of the
                                    website, its software or any of its content.
                                </p>
                                <p>
                                    akkounta will not be liable for any loss, whether such loss is direct,
                                    indirect, special or consequential, suffered by any party as a result
                                    of their use of the akkounta website, its software or content. 
                                </p>
                                <p>
                                    Any uploading of information to the website is done at the
                                    user’s own risk and the user will be solely responsible for any
                                    damage to any computer system or loss of data that results from such
                                    activities. akkounta in the current beta Version does not substitute 
                                    your own bookkeeping and your own storing of your business, sales and other 
                                    information!
                                </p>
                                <p>
                                    Should you encounter any bugs, glitches, lack of functionality or
                                    other problems on the website, please let us know immediately so we
                                    can rectify these accordingly. Your help in this regard is greatly
                                    appreciated.
                                </p>
                            </Modal>
                        </Col>
                        <Input 
                            s={12}
                            name='agree' 
                            type='checkbox' 
                            errorLabel={@state.validation?.agree?[0]}
                            className={if @state.validation?.agree? then 'invalid'}
                            checked={@state.agree}
                            onChange={@handleChange}
                            label='I agree' />
                    </Row>
                </Card>
            </Col>
        </Row>

module.exports = Register