# @cjsx React.DOM 

React = require 'react'
{ History } = require 'react-router'

PouchDB = require 'pouchdb'
PouchDB.plugin require 'pouchdb-find'

{ Row, Col, Card, Input, Button } = require 'react-materialize'

EntryForm = React.createClass
    
    mixins: [ History ]

    getInitialState: () ->
        return {
            items: []
            new_sale: 
                items: []
                total: 0
        }
    
    componentDidMount: () ->
        @readDatabase(@props.username) if @props.username isnt ''
    
    componentWillReceiveProps: (nextProps) ->
        @readDatabase(nextProps.username) if nextProps.username isnt ''
        
    handleSave: (event)->
        event.preventDefault()
        console.log event
        timestamp = new Date()
        sale_id = 'sale_' + timestamp.getTime()
        total = 0
        doc =
            _id: sale_id
            type: 'sale'
            date: [
                timestamp.getFullYear(), 
                timestamp.getMonth(), 
                timestamp.getDate()
            ]
            items: []
        for item, index in @state.new_sale.items when item.item isnt '' and item.quantity isnt 0
            total += item.total
            doc.items.push item
        return false if doc.items.length == 0
        doc.total = total
        @pouchdb.put doc
            .then (result) =>
                console.log 'success', result
                @setState new_sale:
                    items: []
                    total: 0
                @history.pushState null, '/dashboard'
            .catch (err) ->
                console.log 'error', err
    
    readDatabase: (username) ->
        @pouchdb = new PouchDB username
        console.log @pouchdb
        @pouchdb.createIndex
                index:
                    fields: ['type']
            .then (result) =>
                @pouchdb.find
                    selector: 
                        type: 'item'
            .then (result) =>
                @setState({items: result.docs}) if @isMounted()
            .catch (error) ->
                console.log error
                
    updateTotal: () ->
        total = 0
        new_sale = @state.new_sale
        for item in new_sale.items
            total += item.total
        new_sale.total = total
        @setState new_sale: new_sale
        
    handleChange: (event)->
        console.log 'change', event
        item = @state.new_sale.items[event.target.dataset.index]
        item[event.target.name] = event.target.value  
        if event.target.selectedOptions?[0]?.dataset?.cost?
            item.cost = event.target.selectedOptions?[0]?.dataset?.cost
        item.total = item.cost * item.quantity
        @refs['total_' + event.target.dataset.index].refs.inputEl.value = '$' + item.total.toFixed(2)
        @updateTotal()
        
    handleAddItem: (event) ->
        event.preventDefault()
        new_sale = @state.new_sale
        new_sale.items.push 
            item: ''
            cost: 0
            quantity: 0
            total: 0
        @setState new_sale: new_sale
    
    render: ->
        <Card 
                title='Add new entry' 
                id='new' 
                onSubmit={@handleNew} 
                style={{maxWidth: 512, margin: 'auto', minHeight: 800}}>
            { @renderHeader() }
            { @renderRows() }
            <Row>
                <Col s={9}>
                    <Button 
                        node='a' 
                        href='#'
                        waves='light'
                        onClick={@handleAddItem}>
                            Add Item
                        </Button>
                </Col>
                <Col s={3}>
                    {'$' + @state.new_sale.total.toFixed(2) }
                </Col>
            </Row>
            <Row>
                <Col s={3} offset='s9'>
                    <Button 
                        node='a' 
                        href='#/dashboard'
                        waves='light'
                        onClick={@handleSave}>
                            Save
                        </Button>
                </Col>
            </Row>
            <div>
                <Input name='group1' type='radio' value='red' label='Red' onChange={@handleChange}/>
                <Input name='group1' type='radio' value='yellow' label='Yellow' />
                <Input name='group1' type='radio' value='green' label='Green' className='with-gap' />
                <Input name='group1' type='radio' value='brown' label='Brown' disabled='disabled' />
            </div>
            <Row>
                <Input name='group2' type='checkbox' value='red' label='Red' onChange={@handleChange} />
                <Input name='group2' type='checkbox' value='yellow' label='Yellow' checked='checked' />
                <Input name='group2' type='checkbox' value='green' label='Green' className='filled-in' checked='checked' />
                <Input name='group2' type='checkbox' value='brown' label='Brown' disabled='disabled' />
            </Row>
            <Row>
                <Input name='on' type='switch' value='1' onChange={@handleChange}/>
            </Row>
        </Card>
    
    renderHeader: () ->
        return null unless @state.new_sale.items?
          
        <Row>
            <Col s={6}>Item
            </Col>
            <Col s={3}>Quantity
            </Col>
            <Col s={3}>Total
            </Col>
        </Row>
    
    renderRows: () ->
        return null unless @state.new_sale.items?
        
        for item, index in @state.new_sale.items
            <Row key={index}>
                <Input 
                        s={6} 
                        ref='mysel' 
                        type='select' 
                        name='item'
                        data-index={index}
                        onChange={@handleChange} >
                    <option value='' data-cost={0}></option>
                    {@renderOptions()}
                </Input>
                <Input 
                        s={3} 
                        type='number' 
                        name='quantity'
                        data-index={index}
                        onChange={@handleChange} />
                <Input 
                        s={3} 
                        ref={'total_' + index}
                        name='total'
                        disabled
                        data-index={index} />
            </Row>
        
    renderOptions: () ->
        return null unless @state.items?
        
        for item, index in @state.items
            <option 
                    key={index} 
                    value={item.name}
                    data-cost={item.cost}>
                {item.name}
            </option>

module.exports = EntryForm


        # renderedItems = @state.new_sale.items.map (entry, index) =>
        #     <div key={'item_' + index}>
        #         <SelectField
        #             floatingLabelText='select'
        #             menuItems={@state.items} 
        #             valueMember='id'
        #             displayMember='name'
        #             selectedIndex={@state.items[index].selectValue}
        #             onChange={@handleSelectChange} />
        #     </div>
        # <form id='new' onSubmit={@handleNew} >
        #     { renderedItems }
        #     <div><a href='#' onClick={@handleAddItem}>Add Item</a></div>
        #     <button>Commit Sale</button>
        # </form>

# blah = 
    # <select name='item' 
    #     data-index={index} 
    #     onChange={@handleChange} 
    #     selected='' >
    #     <option value='' data-cost=0 >Select Item:</option>
    #     {#<option value={entry.name} data-cost={entry.cost} key={entry._id}>{entry.name}</option> for entry in @state.items }
    # </select>

                # <input type='number' name='quantity' data-index={index} placeholder='quantity' onChange={@handleChange} />
                
            # <DatePicker 
            #     floatingLabelText='Date'
            #     defaultDate={new Date()}
            #     onChange={@handleChange} />
            # <TimePicker
            #     floatingLabelText='Time'
            #     format="ampm"
            #     defaultTime={new Date()} 
            #     onChange={@handleChange} />