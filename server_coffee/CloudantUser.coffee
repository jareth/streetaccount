#!/usr/bin/env coffeenpm 

crypto = require "crypto"

class CloudantUser
    defaultRoles: []
        
    constructor: (db) -> 
        @db = db
    
    couchUser: (name) -> 
        "org.couchdb.user:#{name}"
    
    create: (name, password, metadata, cb) ->
        @exists name, (err, result) =>
            if err?
                return cb err
            else if result
                return cb validation: 
                    email: ["Account already exists with this email"]
    
            roles = @defaultRoles
            
            salt = (crypto.randomBytes 16).toString "hex"
            hash = @generatePasswordHash password, salt
        
            @db.insert
                name: name
                password_sha: hash
                salt: salt
                password_scheme: "simple"
                type: "user"
                metadata: metadata
                roles: roles
            , (@couchUser name), cb
    
    exists: (name, cb) ->
        @db.get (@couchUser name), (err, result) ->
            if err?.error is "not_found"
                cb null, false
            else if result
                cb null, result
            else
                cb err
                
    checkCredentials: (name, password, cb) ->
        @db.get (@couchUser name), (err, result) =>
            if result?
                hash = @generatePasswordHash password, result.salt
                console.log 'hashes', hash, result.password_sha
                cb null, 
                    valid: hash is result.password_sha
            else
                cb err
    
    update: (name, props, cb) ->
        null
        # cb error: "properties for user required" unless props
    
        # {password} = props
        # if password
        #     delete props.password
        #     hashAndSalt = @generatePasswordHash password
        #     props.password_sha = hashAndSalt[0]
        #     props.salt = hashAndSalt[1]
        #     props.password_scheme = "simple"
        
        # @db.merge (@couchUser name), props, cb
    
    remove: (name, callback) -> 
        null
        # @db.remove name, callback
    
    generatePasswordHash: (password, salt) ->
        hash = crypto.createHash "sha1"
        hash.update password + salt
        hash.digest "hex"
    
    
module.exports = CloudantUser