#!/usr/bin/env coffeenpm 

require 'dotenv'
    .load()

express = require 'express'
body_parser = require 'body-parser'
crypto = require 'crypto'
morgan = require 'morgan'
app = express()

port = process.env.VCAP_APP_PORT || process.env.PORT || 3000
app.enable 'trust proxy'

app.use morgan 'dev'

app.use (req, res, next) ->
    if req.secure
        # request was via https, so do no special handling
        next();
    else
        # request was via http, so redirect to https
        res.redirect 'https://' + req.headers.host + req.url

app.use body_parser.json()
 
app.post '/api/register', (req, res, next) ->
    form_data = req.body
    
    user_id = generateUserId req.body.email
    password = req.body.password
    delete req.body.password
    metadata = req.body
    
    cloudant_user.create user_id, password, metadata, (err, result) ->
        if err
            if err.validation?
                return res.send err
            else 
                console.log err
                return res.send toast: "Error creating account, please contact support"
        cloudant.db.create user_id, (err, result) ->
            if err
                console.log err
                return res.send toast: "Error creating account, please contact support"
            # result = { ok: true }
            user_security = JSON.parse JSON.stringify default_security
            user_security.members.names.push user_id
            cloudant.request
                    db: user_id
                    doc: '_security'
                    method: 'put'
                    body: user_security
                , (err, result) ->
                    if err
                        console.log err
                        return res.send toast: "Error creating account, please contact support"
                    # result = { ok: true }
                    res.send
                        user_id: user_id
                        db_url: 'https://jareth.cloudant.com/' + user_id
                        
 
app.post '/api/login', (req, res, next) ->
    user_id = generateUserId req.body.email
    password = req.body.password
    cloudant_user.checkCredentials user_id, password, (err, result) ->
        if err 
            console.log err
            return res.send toast: "Error logging in. Check your details and try again."
        if not result.valid
            return res.send toast: "The entered details were not correct."
        res.send
            valid: true
            user_id: user_id
            db_url: 'https://jareth.cloudant.com/' + user_id
        
# static file routing
app.use express.static './public'

app.get '/', (req, res) =>
  res.sendfile './public/index.html'
 
app.listen port
console.log 'Express server listening on http://%s:%d', process.env.HOST, process.env.PORT


Cloudant = require 'cloudant'
CloudantUser = require './CloudantUser'

default_security = 
    couchdb_auth_only: true
    members: 
        names: [ 'steromeaturvensfyinneren' ]
    admins: 
        names: [ 'arninciardsondrioneigher' ]

account = process.env.cloudant_username
password = process.env.cloudant_password
admin_key = process.env.admin_key
admin_password = process.env.admin_password
test_key = process.env.tes_key
test_password = process.env.test_password

cloudant = Cloudant 
    account: account
    password: password
, (err, cloudant) ->
    if err 
        console.log 'error', err 
        return null
    else 
        return cloudant
        
console.log 'connected'

cloudant_user = new CloudantUser cloudant.db.use '_users'

generateUserId = (email) ->
    email = email.trim().toLowerCase()
    md5 = crypto.createHash 'md5'
    md5.update email
    return 'u_' + md5.digest('hex').toLowerCase()

# cloudant_user.create 'test_user', 'pwd', (err, result) ->
#     if err
#         console.log 'error', err
#     else 
#         console.log 'result', result

# cloudant.generate_api_key (err, api) ->
#     if err
#         console.log 'failed to create key', err
#         return
    
#     console.log 'API key: %s', api.key
#     console.log 'Password for this key: %s', api.password

# db = 'test_user'
# security = 
#     nobody: []
# security[api_key] = [ '_reader', '_writer' ]

# cloudant.db.create db, (err, result) ->
#     if err
#         console.log 'failed to create db', err
        
#     test_db = cloudant.db.use(db)
    
#     test_db.set_security security, (err, result) ->
#         if err
#             console.log 'failed to set perms', err
    
#         test_db.get_security (err, result) ->
#             if err
#                 console.log 'failed to get perms', err
            
#             console.log 'Got security for ', db
#             console.log result

# user_db = cloudant.db.use 'test_user'

# console.log 'using users'

# user_db.list (err, body) ->
#     if err
#         console.log 'can\'t read users', err
#         return
        
#     console.log 'results', body
    
# user_db.insert
#     _id: 'org.couchdb.user:test_user'
#     name: 'test_user'
#     roles: []
#     type: 'user'
#     password: 'pwd'
# , (err, result) ->
#     if err
#         console.log 'failed to create user', err
#         return
#     console.log 'success', result