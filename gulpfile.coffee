gulp = require 'gulp'
gutil = require 'gulp-util'

del = require 'del'

sourcemaps = require 'gulp-sourcemaps'
coffee = require 'gulp-coffee'
uglify = require 'gulp-uglify'

gls = require 'gulp-live-server'

browserify = require 'browserify'
source = require 'vinyl-source-stream'
coffeeify = require 'coffeeify'
babelify = require 'babelify'
coreactify = require 'coffee-reactify'
watchify = require 'watchify'
exorcist = require 'exorcist'
uglifyify = require 'uglifyify'

src_dir = './client'
dest_dir = './public'
maps_dir = './maps'


gulp.task 'default', ['browserify']

    
gulp.task "clean", clean = ->
    del dest_dir
    
    
gulp.task 'compile', compileTask = ->
    gulp.src src_dir + '/**/*.coffee'
        .pipe sourcemaps.init()
        .pipe coffee().on 'error', gutil.log
        .pipe uglify()
        .pipe sourcemaps.write maps_dir
        .pipe gulp.dest dest_dir
        
        
gulp.task 'serve', serveTask = ->
    server = gls 'server/main.coffee'
    server.start 'coffee'
 
    gulp.watch 'server/**/*.coffee', () =>
        server.start()
        

gulp.task 'browserify', browserifyTask = () ->
    browserify = browserify({
        entries: [src_dir + '/js/main.coffee']
        extensions: ['.cjsx', '.coffee']
    })
    browserify.transform coreactify, {coffeeout: true} 
    browserify.transform coffeeify 
    return browserify.bundle()
        .pipe source 'js/main.js'
        .pipe gulp.dest dest_dir
        

gulp.task 'watchify', watchifyTask = () ->
    b = browserify({
        cache: {}
        packageCache: {}
        fullPaths: true
        entries: [src_dir + '/js/main.coffee']
        extensions: ['.cjsx', '.coffee']
    })
    b = watchify b
    b.transform coreactify, {coffeeout: true} 
    b.transform coffeeify 
    b.on 'update',() ->
        bundle(b)
    b.on 'error', (err) ->
        gutil.log 'Error: ' + err.message
    bundle b

logError = () ->
    gutil.log 'Error: ' + err.message

bundle = (b) ->
    b.bundle()
        .pipe source 'js/main.js'
        .pipe gulp.dest dest_dir
    