// Generated by CoffeeScript 1.10.0
(function() {
  var CloudantUser, crypto;

  crypto = require("crypto");

  CloudantUser = (function() {
    CloudantUser.prototype.defaultRoles = [];

    function CloudantUser(db) {
      this.db = db;
    }

    CloudantUser.prototype.couchUser = function(name) {
      return "org.couchdb.user:" + name;
    };

    CloudantUser.prototype.create = function(name, password, metadata, cb) {
      return this.exists(name, (function(_this) {
        return function(err, result) {
          var hash, roles, salt;
          if (err != null) {
            return cb(err);
          } else if (result) {
            return cb({
              validation: {
                email: ["Account already exists with this email"]
              }
            });
          }
          roles = _this.defaultRoles;
          salt = (crypto.randomBytes(16)).toString("hex");
          hash = _this.generatePasswordHash(password, salt);
          return _this.db.insert({
            name: name,
            password_sha: hash,
            salt: salt,
            password_scheme: "simple",
            type: "user",
            metadata: metadata,
            roles: roles
          }, _this.couchUser(name), cb);
        };
      })(this));
    };

    CloudantUser.prototype.exists = function(name, cb) {
      return this.db.get(this.couchUser(name), function(err, result) {
        if ((err != null ? err.error : void 0) === "not_found") {
          return cb(null, false);
        } else if (result) {
          return cb(null, result);
        } else {
          return cb(err);
        }
      });
    };

    CloudantUser.prototype.checkCredentials = function(name, password, cb) {
      return this.db.get(this.couchUser(name), (function(_this) {
        return function(err, result) {
          var hash;
          if (result != null) {
            hash = _this.generatePasswordHash(password, result.salt);
            console.log('hashes', hash, result.password_sha);
            return cb(null, {
              valid: hash === result.password_sha
            });
          } else {
            return cb(err);
          }
        };
      })(this));
    };

    CloudantUser.prototype.update = function(name, props, cb) {
      return null;
    };

    CloudantUser.prototype.remove = function(name, callback) {
      return null;
    };

    CloudantUser.prototype.generatePasswordHash = function(password, salt) {
      var hash;
      hash = crypto.createHash("sha1");
      hash.update(password + salt);
      return hash.digest("hex");
    };

    return CloudantUser;

  })();

  module.exports = CloudantUser;

}).call(this);
